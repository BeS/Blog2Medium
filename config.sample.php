<?php
/**
 * @copyright Copyright (c) 2017 Bjoern Schiessle <bjoern@schiessle.org>
 *
 * @license GNU GPL version 3 or any later version
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

$config = [
	/** Medium API configuration */
	'mediumId' => '',
	'mediumAccessToken' => '',/** licence of yor articles */
	'license' => 'cc-40-by-sa',
	/** should your medium.com followers be notified? */
	'notifyFollowers' => true,
	/** Blog Feed */
	'rssFeed' => 'https://www.schiessle.org/index.xml',
	/** parent which contain all your articles, can be different from RSS feed to RSS feed, check the RSS feed to find the right structure */
	/** in this example all articles are located at $rssFeed['channel']['item'] */
	'articles' => ['channel', 'item'],
	/** where is the title stored in the RSS */
	'title' => 'title',
	/** where is the actual article stored in the RSS */
	'content' => 'description',
	/** where is the list of tags stored in the RSS */
	'tags' => 'category',
	/** where is the link to the article stored in the RSS */
	'canonicalUrl' => 'link',
];
