<?php
/**
 * @copyright Copyright (c) 2017 Bjoern Schiessle <bjoern@schiessle.org>
 *
 * @license GNU GPL version 3 or any later version
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */


require 'config.php';

class Blog2Medium {

	/** @var  string */
	private $rssFeed;

	/** @var array the parent of all the blog articles  */
	private $startingPoint;

	/** @var string under this tag we find the article title */
	private $titleTag;

	/** @var string under this tag we find the article */
	private $contentTag;

	/** @var string under this tag we find the article tags */
	private $tagsTag;

	/** @var string under this tag we find the link to the article */
	private $canonicalUrlTag;

	/** @var string */
	private $license;

	/** @var bool */
	private $notifyFollowers;

	/** @var string */
	private $mediumId;

	/** @var string */
	private $mediumAccessToken;

	/** @var bool mark all feed entries as processed but don't post them to medium */
	private $skipAll;

	/** @var  bool output result but don't post to medium */
	private $dryRun;

	/** @var  array of already posted items */
	private $history;

	/** @var string the file which stores already posted items */
	private $historyFile = 'history.json';

	/** @var string path to history file */
	private $historyDir;

	/** @var string url to Medium API */
	private $mediumAPI;

	/**
	 * Blog2Medium constructor.
	 *
	 * @param array $config
	 * @throws Exception
	 */
	public function __construct(array $config, $dryRun, $skipAll) {
		$this->mediumId = $config['mediumId'];
		$this->mediumAccessToken = $config['mediumAccessToken'];
		$this->rssFeed = $config['rssFeed'];
		$this->startingPoint = $config['articles'];
		$this->titleTag = $config['title'];
		$this->contentTag = $config['content'];
		$this->tagsTag = $config['tags'];
		$this->canonicalUrlTag = $config['canonicalUrl'];
		$this->license = $config['license'];
		$this->notifyFollowers = $config['notifyFollowers'];
		$this->skipAll = $skipAll;
		$this->dryRun = $dryRun;
		$this->mediumAPI = "https://api.medium.com/v1/users/".$this->mediumId."/posts";

		$home = getenv("HOME");
		$this->historyDir = $home . '/.local/share/Blog2Medium';

		$this->loadHistory();

		$feed = $this->parseRssFeed();
		$this->postToMedium($feed);
		$this->storeHistory();

	}

	/**
	 * load already posted blog articles from file
	 */
	private function loadHistory() {

		$this->history = [];

		if (!is_dir($this->historyDir)) {
			mkdir($this->historyDir, 0755, true);
		}

		$path = $this->historyDir . '/' . $this->historyFile;
		if (file_exists($path)) {
			$fileContent = file_get_contents($path);
			$this->history = json_decode($fileContent, true);
		}
	}

	/**
	 * store list of already send blog posts
	 */
	private function storeHistory() {
		if ($this->dryRun) return;

		$path = $this->historyDir . '/' . $this->historyFile;
		$text = json_encode($this->history);
		file_put_contents($path, $text);
	}

	/**
	 * parse atom feed and return array of posts
	 *
	 * @return array
	 * @throws Exception
	 */
	public function parseRssFeed() {
		$result = [];
		$feed = implode(file($this->rssFeed));
		$xml = simplexml_load_string($feed);
		$json = json_encode($xml);
		$array = json_decode($json,true);
		$articles = $this->getArticlesArray($array);
		foreach ($articles as $item) {
			if (empty($item['description']) || empty($item['title']) || empty($item['link']) || !isset($item['category'])) continue;
			$url = $item[$this->canonicalUrlTag];
			$result[$url] = [
				'title' => $item[$this->titleTag],
				'content' => $item[$this->contentTag],
				'canonicalUrl' => $url,
				'tags' => $item[$this->tagsTag],
				'license' => $this->license,
				'notifyFollowers' => $this->notifyFollowers,
				'contentFormat' => 'html'
			];
		}
		return array_reverse($result);
	}

	/**
	 * get this parent of the RSS array where all the blog posts are located
	 *
	 * @param array $rss
	 * @return array
	 * @throws Exception
	 *
	 */
	private function getArticlesArray(array $rss) {
		foreach ($this->startingPoint as $index) {
			if (isset($rss[$index])) {
				$rss = $rss[$index];
			} else {
				throw new Exception('can not find blog articles, please check your configuration!');
			}
		}

		if (is_array($rss)) {
			return $rss;
		}

		throw new Exception('can not find blog articles, please check your configuration!');
	}

	/**
	 * post rss feed to Medium
	 *
	 * @param array $articles
	 */
	private function postToMedium(array $articles) {
		foreach ($articles as $url => $article) {
			if (!isset($this->history[$url])) {
				// only output the message but don't post it
				if ($this->dryRun) {
					echo "\n" . $article['title'] . "\n";
					continue;
				}
				$this->history[$url] = $article['title'];
				// mark as read but don't post
				if ($this->skipAll) {
					continue;
				}

				// send article to medium
				$article["content"].= '<p><i>This was originally published <a href="'.$article["canonicalUrl"].'" rel="canonical">on my own site</a>.</i></p>';
				$headers = array(
					"Authorization: Bearer ".$this->mediumAccessToken,
					"Content-Type: application/json",
					"Accept: application/json",
					"Accept-Charset: utf-8"
				);
				$options = array(
					CURLOPT_URL => $this->mediumAPI,
					CURLOPT_RETURNTRANSFER => TRUE,
					CURLOPT_POST => TRUE,
					CURLOPT_HTTPHEADER => $headers,
					CURLOPT_POSTFIELDS => json_encode($article),
					CURLOPT_TIMEOUT => 20
				);
				$curl = curl_init();
				curl_setopt_array($curl, $options);
				$response = curl_exec($curl);
				$responseArray = json_decode($response, true);
				curl_close($curl);

				if (isset($responseArray['data']['publishedAt'])) {
					// article published successfully
					return;
				}

				// article was not published, remove it from history again
				unset($this->history[$url]);

			}
		}
	}

}

$dryRun = false;
$skipAll = false;

foreach($argv as $value) {
	if ($value === '-d' || $value === '--dry-run') {
		$dryRun = true;
	}
	if ($value === '-s' || $value === '--skip') {
		$skipAll = true;
	}
	if ($value === '-h' || $value === '--help') {
		echo "\nRun: php blog2medium.php\n\n";
		echo "Optional parameters:\n";
		echo "-s | --skip    : mark all existing blog articles as processed but don't send them to medium.com\n";
		echo "-d | --dry-run : don't post anything to medium.com, just output what would have been send\n";
		echo "-h | --help    : output help\n\n";
		exit();
	}
}

try {
	$b2m = new Blog2Medium($config, $dryRun, $skipAll);
} catch (\Exception $e) {
	print "\nSomething went wrong: " . $e->getMessage() . "\n";
}
