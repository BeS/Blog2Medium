# Blog2Medium

This is a small tool which posts your Blog posts to medium.com

## Setup

To get a ID and token for Medium login to medium.com and go to your settings.
Scrolled all the way to the bottom to the heading "Integration tokens". 
Entered a description and pressed the "Get integration token" button.
Now use that token to get the second piece of information: your user ID.
Open your browser and go to this URL: `https://api.medium.com/v1/me?accessToken=<your new secret integration token>`.
That returns a JSON response. One of the fields in the JSON object has the name "id". 
The value of that field is your user ID.


Copy the `config.sample.php` to `config.php` and adjust the config values.

## Run

Running the program is as easy as `php blog2medium.php`. Setup a cronjob
to check your feed for new blog posts regularly and forward them.

If you want to test your setup without sending anything to medium.com you can call
`php blog2medium.php --dry-run`. This will output all the RSS feed items 
which would have been send to medium.com otherwise.

To avoid that on first run all your old blog posts will be send to medium.com
you can call `php blog2medium.php --skip`. This way all existing RSS feed
items will be marked as processed but not send to medium.com. Only blog posts created
after you run the command will be send to medium.com.

`php blog2medium.php --help` gives you a overview of all commandline
arguments.
